import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
	
@RunWith(Parameterized.class)
public class VocalsV2FraseConVocales {

	private String fraseNormal;
	private int contador;
	private char vocal;
	private String fraseFinal;

	public VocalsV2FraseConVocales(String fraseNormal,  char vocal, int contador,String fraseFinal) {
		this.fraseNormal = fraseNormal;
		this.contador = contador;
		this.vocal = vocal;
		this.fraseFinal = fraseFinal;

	}

	@Parameters
	public static Collection<Object[]> numeros() {
		return (Collection<Object[]>) Arrays.asList(new Object[][] { { "murcielago", 'a', 1, "marcaalaga" },
				{ "murcielago", 'e', 2, "merceelege" }, { "murcielago", 'i', 3, "mirciiligi" },
				{ "murcielago", 'o', 4, "morcoologo" }, { "murcielago", 'u', 5, "murcuulugu" }

		});

	}

	@Test
	public void VocalsV2FraseConVocales() {
		String res = VocalsV2.FraseConVocales(fraseNormal, vocal, contador);
		assertEquals(fraseFinal, res);

	}

}
