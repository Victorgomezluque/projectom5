import java.util.Scanner;

/**
 * 
 * @author V�ctor G�mez Luque
 * @version 22/2/2019V1 
 * 
 * Aquest programa serveix per escriure una frase i canviem totes les vocals per una vocal nomes.
 */

public class Vocals {
	static Scanner sc = new Scanner(System.in);

	/**
	 * @param args
	 * Aqui truquem al metode FraseConVocales per fer tot el programa.
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String fraseNormal = null;
		char vocal = 0;
		int contador = 1;
		int frases;
		int contadorfrases = 0;

		frases = sc.nextInt();
		frase(fraseNormal);
		while (contadorfrases != frases) {
			FraseConVocales(fraseNormal, vocal, contador);
			contadorfrases++;
		}

	}

	/**
	 * 
	 * @param fraseNormal
	 *            - La frase on cambiarem les vocals.
	 * @return La frase que separarem.
	 * 
	 * Aquest metode serveix per llegir una frase.
	 */
	public static String frase(String fraseNormal) {

		fraseNormal = sc.nextLine();
		return fraseNormal;

	}

	/**
	 * 
	 * @param vocal
	 *            - �s per la vocal que cambiarem les altres vocals.
	 * @param contador
	 *            - Per contar per la vocal que es cambiara les altres vocals.
	 * @return - retorna una vocal.
	 * 
	 *         El metode ho utilitzem alhora de cambiar totes les vocals per una a
	 *         la frase.
	 */
	public static char Vocal(char vocal, int contador) {

		switch (contador) {
		case 1:
			vocal = 'a';
			contador++;
			break;
		case 2:
			vocal = 'e';
			contador++;
			break;

		case 3:
			vocal = 'i';
			contador++;
			break;

		case 4:
			vocal = 'o';
			contador++;
			break;

		case 5:
			vocal = 'u';
			contador++;
			break;

		}
		return vocal;

	}

	/**
	 * 
	 * @param fraseNormal
	 *            - La frase on cambiarem les vocals.
	 * @param vocal
	 *            - �s per la vocal que cambiarem les altres vocals.
	 * @param contador
	 *            - Per contar per la vocal que es cambiara les altres vocals.
	fraseSeparada - Utilitzarem aquest parametre separar la frase normal.
	 *            
	 *Aquest metode truquem els metodes frase i vocal, anem contant les lletres una a una i si es una Vocal miniscula o mayuscula la canvien per la vocal que toca.
	 */
	
	public static void FraseConVocales(String fraseNormal, char vocal, int contador) {
		fraseNormal = frase(fraseNormal);

		char[] fraseSeparada = new char[fraseNormal.length()];
		int i;

		while (contador != 6) {
			vocal = Vocal(vocal, contador);
			for (i = 0; i != fraseNormal.length(); i++) {

				fraseSeparada[i] = fraseNormal.charAt(i);

				switch (fraseSeparada[i]) {
				case 'a':
				case 'A':
					fraseSeparada[i] = vocal;

					break;
				case 'e':
				case 'E':
					fraseSeparada[i] = vocal;
					break;

				case 'i':
				case 'I':
					fraseSeparada[i] = vocal;

					break;

				case 'o':
				case 'O':
					fraseSeparada[i] = vocal;

					break;

				case 'u':
				case 'U':
					fraseSeparada[i] = vocal;

					break;

				}
			}
			contador++;
			System.out.println("Frase amb la Vocal " + vocal + ":  ");

			for (i = 0; i < fraseNormal.length(); i++) {

				System.out.print(fraseSeparada[i]);
			}
			System.out.println(" ");
			System.out.println(" ");
		}

	}

}
