import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ Vocal.class, VocalsV2FraseConVocales.class })
public class AllTests {

}
