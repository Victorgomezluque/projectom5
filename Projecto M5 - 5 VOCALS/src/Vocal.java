import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class Vocal {

	private int contador;
	private char vocal;

	public Vocal(char vocal, int contador) {

		this.contador = contador;
		this.vocal = vocal;

	}

	@Parameters
	public static Collection<Object[]> numeros() {
		return (Collection<Object[]>) Arrays.asList(new Object[][] { 
			{ 'a',1 }, 
			{ 'e', 2 }, 
			{ 'i', 3 },
			{ 'o', 4 },
			{ 'u', 5 }

		});

	}

	@Test
	public void VocalsV2FraseConVocales() {
		char res = VocalsV2.Vocal(vocal, contador);
		assertEquals(vocal, res);

	}

}
