import java.util.Scanner;



public class VocalsV2 {
	static Scanner sc = new Scanner(System.in);

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String fraseNormal = null;
		char vocal = 0;
		int contador = 1;
		int frases;
		int contadorfrases = 0;
		int k;

		frases = sc.nextInt();
		sc.nextLine();

		while (contadorfrases != frases) {
			fraseNormal = frase();

			for (k = 1; k <= 5; k++) {
				String fraseCambiada = FraseConVocales(fraseNormal, vocal, k);
				mostrar(fraseCambiada);

			}
			contadorfrases++;
		}

	}

	public static void mostrar(String fraseCambiada) {

		System.out.println(fraseCambiada);

	}


	public static String frase() {
		String fraseNormal;

		fraseNormal = sc.nextLine();

		return fraseNormal;

	}

	
	public static char Vocal(char vocal, int contador) {

		switch (contador) {
		case 1:
			vocal = 'a';
			break;
		case 2:
			vocal = 'e';
			break;

		case 3:
			vocal = 'i';
			break;

		case 4:
			vocal = 'o';
			break;

		case 5:
			vocal = 'u';
			break;

		}
		return vocal;

	}



	public static String FraseConVocales(String fraseNormal, char vocal, int contador) {
		char[] fraseSeparada = new char[fraseNormal.length()];
		int i;

		vocal = Vocal(vocal, contador);
		for (i = 0; i != fraseNormal.length(); i++) {

			fraseSeparada[i] = fraseNormal.charAt(i);

			switch (fraseSeparada[i]) {
			case 'a':
			case 'A':
				fraseSeparada[i] = vocal;

				break;
			case 'e':
			case 'E':
				fraseSeparada[i] = vocal;
				break;

			case 'i':
			case 'I':
				fraseSeparada[i] = vocal;

				break;

			case 'o':
			case 'O':
				fraseSeparada[i] = vocal;

				break;

			case 'u':
			case 'U':
				fraseSeparada[i] = vocal;

				break;

			}

		}

		return String.copyValueOf(fraseSeparada);

	}

}
